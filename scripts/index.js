var enable_bnews_click=0;
var now = new Date();
var pageHistory = [];
var isFromSearch = 0;
var myKeyWord = '';
var myArticleLead = [];
var isGalleryLoaded = 0;
var jsonBaseFeed = "http://tab.thehindu.com/hinduipad/";
var templateBaseFeed = "";
var hinduWebBaseURL = "http://www.thehindu.com/";
var currentArticleId = "";
var currentSectionId = "";
var prevSectionId = "";
var prevArticleId = "";
var currentScreen = "";
var prevScreen = "";
var sectionInWords = "";
var galleryPos = 0;
var galleryArrIds = [];
var galleryImageIDStuck = [];
var galleryImageId = [];
var galCount = 0;
var actDataColHome = [];
var actDataSecColHome = [];
var myArticleData = [];
var myArticleJson = [];
var myTitleData = [];
var myArticleImages = [];
var myArticleImageCaption = [];
var myArticlePubDate = [];
var myArticleByLine = [];
var myArticleRelnewsTitle = [];
var myArticleWebURL = [];
var myArticleComment=[];
var bNewInterval;
var tapHoldInterval;
var savedArticles = [];
var savedImages = [];
var myScroll;
var tabScroll;
var articleScroll;
var navScroll;
var imgGallery;
var imgSecGallery;
var pullDownEl, pullDownOffset,	pullUpEl, pullUpOffset;
var manageSectionScroll;
var activePageArray=[];
var currentLi;
var initialWidth = 0;
var wrpPosition;
var initialSubsecWidth=0;
var currentselectedsubsection=0;
var myCommentsScroll = "";
var citySelected=0;
var isGalleryPresent=0;
//=========Caching the jquery objects========================================
var overlayDivvar=$('#overlayDiv');
var notificationvar=$('#notification');
var wrappervar=$("#wrapper");
var headerwrapvar=$("#header-wrap");
var searchvar=$("#search");
var tabvar=$("#tabs");
var notificationvar=$('#notification');
var homecontainervar=$("#homeContainer");
var homebtnvar=$("#homeBtn");
var gallerycontaineronevar=$("galleryContainerOne");
var backscreenvar=$("#backScreen");
var navtabvar=$("#navTab");
var tiptipvar=$("#tiptip_holder");
var subSecContainervar=$("#subSecContainer");
//============================================================================

//setTimeStamp() starts here
function setTimeStamp(){
	now = new Date();
	$("#updatedDate").html("Last updated "+dateFormat(now, "h:MMtt, ddd, mmmm dS, yyyy"));
}

//setTimeStamp() ends here
//convertHTML() starts here
function convertHTML(myData){
	return $("<span />", { html: myData }).text();
}
//convertHTML() ends here

//hideAllWindow() starts here
function hideAllWindow() {
	tabvar.hide();
	$("#tiptip_holder").fadeOut();
	overlayDivvar.hide();
	$('.dropdown').removeClass("active");
	$('#dd').removeClass('active');
	$('#dd').removeClass('bg');
	
	
}	
//hideAllWindow() ends here

//pullDownAction() starts here
	function pullDownAction () {
			 if(currentScreen!="K"){
			 	if (myScroll) {
				pulldownvar.hide();
				myScroll.destroy();
				myScroll = null;
			}
		refereshHomeScreen();
			 }
			 else {

			 	myScroll.refresh();
			 }
		
	}
//pullDownAction() ends here

//loaded() starts here	
	function loaded() {
		myScroll = new iScroll('wrapper', {
			useTransition: true,
			topOffset: pullDownOffset,
		});
	}
//loaded() ends here	

	document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
	document.addEventListener('DOMContentLoaded', loaded, false);

//refreshHomeScreen() starts here	
	function refereshHomeScreen() {
		setTimeStamp();
		if(currentScreen == "S") {
			isGalleryLoaded = 0;
			if(myScroll){
				myScroll.destroy();
				myScroll = null;
			}
			loadSectionScreen(currentSectionId);
		}
		else if(currentScreen == "A") {
			loadArticleScreen(currentSectionId, currentArticleId);
		}
		else if(currentScreen == "K"){
			currentScreen = prevScreen;
			if(currentScreen!="H"){
				$("#articleContainer").fadeIn();
			} 
			refereshHomeScreen();
		}
		else {
			currentScreen = "H";
			getHomeMainData();
			
			getHomeSectionData();
			clearInterval(bNewInterval);
			getArticle( refreshSavedArtciels );
getGallery();
			getSectionMenu();
			isGalleryLoaded = 0;
			homecontainervar.css("visibility", "hidden");
			backscreenvar.css("visibility", "hidden");
		}
	}
//refreshHomeScreen() ends here

//getInitData() starts here
	function getInitData(){
		$('#iFrameAd').attr('src', 'http://wap.bonzai.mobi/req?medium=web&zone=1365&referer="Home"&weblink='+encodeURIComponent(location.href)).load(function(){resizeContentWindow();}).error(function(){$('#advt').hide();resizeContentWindow();});
		enable_bnews_click = 0;
		$("#header, wrappervar").css({ opacity: 0 });
		setTimeStamp();
		$("#header, wrappervar").css({ opacity: 1 });
		currentScreen = "H";
		setSections();
		getHomeMainData();
getGallery();
		
		clearInterval(bNewInterval);
		getHomeSectionData();
		getSectionMenu();
		getArticle( refreshSavedArtciels );
		backscreenvar.css("visibility", "hidden");
	}
//getInitData() starts here

//backscreen() starts here	
function backScreen(){
		hideAllWindow();
		 var pageArrayLength="";
            activePageArray.pop();
		  pageArrayLength=activePageArray.length-1;
		if(pageArrayLength>-1){
        if(activePageArray[pageArrayLength].currentScreen=="R"){
        		if(currentScreen=="K"){
                $("#articleContainer").empty().html("");
                $('#sectionContainer').empty().html("").fadeOut();;
                $("#articleContainer").fadeIn();
                if(myScroll){
                    myScroll.destroy();
                }
            }
            
            loadArticleScreen(activePageArray[pageArrayLength].sectionId,activePageArray[pageArrayLength].articleId,"B");
             }
            if(activePageArray[pageArrayLength].currentScreen=="A"){
            	if(currentScreen=="K"){
                $("#articleContainer").empty().html("").fadeIn();
            }
            		
                loadArticleScreen(activePageArray[pageArrayLength].sectionId,activePageArray[pageArrayLength].articleId,"B");
                if(myScroll){
                myScroll.destroy();
                myScroll=null;
                }
            }

            if(activePageArray[pageArrayLength].currentScreen=="S"){
                 
                 if(currentScreen=="K"){
                $("#articleContainer").empty().html("").fadeOut();
                $("#articleContainer").fadeOut();
                 $('#sectionContainer').empty().html("").fadeIn();
                $('#sectionContainer').fadeIn();
            }
               
            isGalleryLoaded = 0;
            if(myScroll){
            	myScroll.destroy();
            	myScroll = null;
            }
                loadSectionScreen(activePageArray[pageArrayLength].sectionId,"B");
            }

            if(activePageArray[pageArrayLength].currentScreen=="H"){
                  isGalleryLoaded = 0;

                showHome();
            }

            if(activePageArray[pageArrayLength].currentScreen=="K"){
                    homecontainervar.hide();
                     gallerycontaineronevar.empty().hide();
                    $("#articleContainer").empty().html("").fadeOut();
                    $('#sectionContainer').empty().html("").fadeOut();
                    hideAllWindow();
            }
            
            if(activePageArray[pageArrayLength].currentScreen=="KA"){

               if(currentScreen=="K"){
                 $('#sectionContainer').empty().html("").fadeOut();
                $("#articleContainer").empty().html("").fadeIn();;
            }

            loadSearchScreen(activePageArray[pageArrayLength].sectionId,activePageArray[pageArrayLength].articleId,"B");
            
            }
            
        }		  
	  
	}
	//bacscreen() ends here

//showHome() starts here
	function showHome(){
		//$("#cityTitle").text("City News");
		$("#TabNav li").removeClass("active");
		$("#subSecContainer").hide();	
		$("#imgGalleryTeaser").show();
		$("#secNameDisplay").text("HOME");
		$('#advt').show(function(){
			$('#iFrameAd').attr('src', 'http://wap.bonzai.mobi/req?medium=web&zone=1365&referer="Home"&weblink='+encodeURIComponent(location.href)).load(function(){resizeContentWindow();}).error(function(){$('#advt').hide();resizeContentWindow();});
		});
		if (isFromSearch == 0) {
		};
		isFromSearch = 0;
		homebtnvar.css("visibility", "hidden");
		currentScreen = "H";
		 gallerycontaineronevar.empty().hide();
		gallerycontaineronevar.html("").hide();
		$("#sectionContainer").empty().html("").hide();
		$("#articleContainer").html("").hide();
		homecontainervar.show();
		$("#sectionNameDisp").html("Home");
		backscreenvar.css("visibility", "hidden");
		if(!myScroll){ 
			loaded();
		}
		 activePageArray = [];
		activePageArray.push({
                             articleId:"",
                             sectionId:"",
                             parentArticleId:"",
                             searchKeyWrd:"",
                             currentScreen:"H"
                             });
		$('article').die("click").live('click',function(event) {
			if (myScroll) {
				myScroll.destroy();
				myScroll = null;
			}		
			currentSectionId = $(this).attr("secId");
			currentArticleId = $(this).attr("artId");
			homecontainervar.hide();
			 gallerycontaineronevar.html("").hide().empty().hide();
			$("#articleContainer").fadeIn();
			currentScreen = "A";
			loadArticleScreen(currentSectionId, currentArticleId);
		});
		
		myScroll.refresh();
	}
//showHome() ends here

//showSplash() starts here
	function showSplash(){
		$.getJSON( jsonBaseFeed + "splash?callback=?", function(json, textStatus){
			if(json.data.availability == 1){
				if(json.data.thumbnail != ""){
					$("#splashAd").show();
					$("<a/>").attr("href",json.data.url).attr("target","_blank").attr("id","splashA").appendTo("#splashAd");
					$("<img/>").attr("src", json.data.thumbnail).appendTo("#splashA");
					setTimeout(function () { $("#splashAd").fadeOut(); $("#header, wrappervar").css({ opacity: 1 }); }, json.data.delaytime);
				}
			} else {
				$("#header, wrappervar").css({ opacity: 1 }); 
			}
		});
	}

//showSplash() ends here	

function getHomeMainData(){
 gallerycontaineronevar.empty().hide();
		isFromSearch = 0;    
            activePageArray = [];
        activePageArray.push({
                             articleId:"",
                             sectionId:"",
                             parentArticleId:"",
                             searchKeyWrd:"",
                             currentScreen:"H"
                             });
		backscreenvar.hide();
		$("#dataMain").empty();
		homecontainervar.css("visibility", "hidden");

		$.getJSON( jsonBaseFeed + "homesection?service=homelivemain&callback=?", 
				  function(json, textStatus){
		  	var container = "dataMain";
		  	$.each(json.data, function(j,data){
				if(j==0){
					$(".main_article").show();
					$("<article/>").attr("id","main"+j).attr("secId",data.sectionId).attr("artId",data.articleId).appendTo("#"+container);
					$("<div>").addClass("art_content").attr("id","mainDiv"+j).appendTo("#main"+j);
					if(data.thumbnail != ""){
						$("<img/>").attr("src", data.thumbnail).attr("width","200").appendTo("#mainDiv"+j);
					}
					$("<h2/>").html(convertHTML(data.title)).appendTo("#mainDiv"+j);
				} else {
					$("<article/>").attr("id","main"+j).attr("secId",data.sectionId).attr("artId",data.articleId).appendTo("#"+container);
					$("<div>").addClass("art_content").attr("id","mainDiv"+j).appendTo("#main"+j);
					if(data.thumbnail != ""){
						$("<img/>").attr("src", data.thumbnail).attr("width","85").appendTo("#mainDiv"+j);
					}
					$("<h2/>").html(convertHTML(data.title)).appendTo("#mainDiv"+j);
				}
				$("<time/>").text(data.pubDate).appendTo("#mainDiv"+j);
				$("<p/>").html(convertHTML(data.description)).appendTo("#mainDiv"+j);
		  	});			
		});
	}
//showSplash() ends here

//getHomeSectionData() starts here
	function getHomeSectionData() {
		var datacolvar=0;
        actDataColHome = [];
		$("#loading").css('width','0').show();
		$("#dataCol-1,#dataCol-2,#dataCol-3,#dataCol-4,#dataCol-5,#promoad").empty();
		$.getJSON( jsonBaseFeed + "homesection?service=homelivesecbk&callback=?", function(json, textStatus){
			$.each(json.data, function(i,data){
				if(data.landscape == "left-sub1"){
					myLContainer = "dataCol-1";
				}
				
				if(data.landscape == "left-sub2"){
					myLContainer = "dataCol-2";
				}
				
				if(data.landscape == "center-col1"){
					myLContainer = "dataCol-3";
				}

				if(data.landscape == "center-col2"){
					myLContainer = "dataCol-4";
				}
				
				if(data.landscape == "right-col1"){
					myLContainer = "dataCol-5";
                   
                   actDataColHome.push({
                                       secId:"secId"+i
                                       });
                   
                   
				}
				
				if(i==json.data.length-1){
					$("#loading").hide();
					homecontainervar.css("visibility", "visible").show();
					if(!myScroll){
						loaded();
					}
					myScroll.refresh();
				}
				if(data.landscape == "promoad"){
					if(data.sectionId != ""){
						$("<img/>").attr("src", data.thumbnail).appendTo("#promoad").click(function(){
								currentScreen = "S";
								event.preventDefault();
								currentSectionId = data.sectionId;
								$("#sectionNameDisp").html($(this).html());
								homecontainervar.hide();
								 gallerycontaineronevar.empty().hide();
								$("#articleContainer").fadeOut();
								isGalleryLoaded = 0;
								loadSectionScreen(currentSectionId);
						});
					} else if(data.url != "") {
						$("<a/>").attr("href",data.url).attr("target","_blank").attr("id","promo"+i).appendTo("#promoad");
						$("<img/>").attr("src", data.thumbnail).appendTo("#promo"+i);
					}
					
				}

				//sectionName
				else {

					if(i==0)
					{
						// alert("inside yes");
						
						// $("#dataCol-3 article:first").hide();
						// $("#dataCol-4 article:first").hide();
						datacolvar=i;
							$("<article/>").attr("id","secId"+i).attr("secId",data.sectionId).attr("artId",data.articleId).appendTo("#dataColExtra-1");
					$("<div>").addClass("art_content").attr("id","secDiv"+i).appendTo("#secId"+i);
					if(data.sectionName == "Editorial"){//Editorial
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
					 }
					if(data.sectionName == "Lead"){
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
						}
						if(data.thumbnail != ""){
						$("<img/>").attr("src", data.thumbnail).attr("width","155").appendTo("#secDiv"+i).load(function(){ 
						   		if(myScroll) myScroll.refresh(); 
						   });
					}
					$("<h2/>").html(convertHTML(data.title)).appendTo("#secDiv"+i);
					$("<time/>").text(data.pubDate).appendTo("#secDiv"+i);
					$("<p/>").html(convertHTML(data.description)).appendTo("#secDiv"+i);
					//break;
						
//break;
					}
					
//break;			

					if(i==1)
					{
						
						// $("#dataCol-3 article:first").hide();
						// $("#dataCol-4 article:first").hide();
							datacolvar=i;
							$("<article/>").attr("id","secId"+i).attr("secId",data.sectionId).attr("artId",data.articleId).appendTo("#dataColExtra-2");
					$("<div>").addClass("art_content").attr("id","secDiv"+i).appendTo("#secId"+i);
					if(data.sectionName == "Editorial"){//Editorial
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
					 }
					if(data.sectionName == "Lead"){
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
						}
						if(data.thumbnail != ""){
						$("<img/>").attr("src", data.thumbnail).attr("width","155").appendTo("#secDiv"+i).load(function(){ 
						   		if(myScroll) myScroll.refresh(); 
						   });
					}
					$("<h2/>").html(convertHTML(data.title)).appendTo("#secDiv"+i);
					$("<time/>").text(data.pubDate).appendTo("#secDiv"+i);
					$("<p/>").html(convertHTML(data.description)).appendTo("#secDiv"+i);

						
					}
					if(i>1){
					$("<article/>").attr("id","secId"+i).attr("secId",data.sectionId).attr("artId",data.articleId).appendTo("#"+myLContainer);
					$("<div>").addClass("art_content").attr("id","secDiv"+i).appendTo("#secId"+i);
					if(data.sectionName == "Editorial"){//Editorial
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
					 }
					if(data.sectionName == "Lead"){
						$("<h3/>").html(data.sectionName).appendTo("#secDiv"+i);
						}
						if(data.thumbnail != ""){
						$("<img/>").attr("src", data.thumbnail).attr("width","155").appendTo("#secDiv"+i).load(function(){ 
						   		if(myScroll) myScroll.refresh(); 
						   });
					}
					$("<h2/>").html(convertHTML(data.title)).appendTo("#secDiv"+i);
					$("<time/>").text(data.pubDate).appendTo("#secDiv"+i);
					$("<p/>").html(convertHTML(data.description)).appendTo("#secDiv"+i);
					}
				}
							});

			if(textStatus == "success"){
				myScroll.refresh();
			}
			$("#loading").hide();
			homecontainervar.css("visibility", "visible");
			enable_bnews_click=1;
		});
	}
//getHomeSectionData() ends here

//getGallery() starts here
	function getGallery(){
		$("#galleryUL").html("");
		$("#imgGalleryTeaser").hide();
		galleryArrIds = [];
		$.getJSON( jsonBaseFeed + "photogallery?callback=?&photo=home", function(json, textStatus){
			if(json.data[0].media>0)
			{
				isGalleryPresent=1;
			}
			$.each(json.data[0].media, function(j,data){
				if(j == 1) $("#imgGalleryTeaser").show();
				galleryArrIds[j] = data.articleid;
				if(j==0) $(".gallery").show();
                $("<li/>").attr("id","liImg"+j).appendTo("#galleryUL");
                $("<img/>").attr("src",data.thumbnail).attr("height","200").attr("id","gImg"+j).appendTo("#liImg"+j);
                $("<div/>").attr("id","galleryDiv1"+j).addClass("caption").appendTo("#liImg"+j).unbind("click").click(function(){
                            if(myScroll){
                              myScroll.destroy
                               myScroll.destroy();
                               myScroll = null;
                                }
                            $(".news_headlines").hide();
                            loadGalleryPage(galleryArrIds[galleryPos]);      
                        });

          $("<h3/>").html(convertHTML(data.title)).appendTo("#galleryDiv1"+j);
          $("<p/>").html(convertHTML(data.caption)).appendTo("#galleryDiv1"+j);
		  	});
			          $("#gHeader").unbind('click').click(function(event){
                             if(myScroll){
                              myScroll.destroy
                              myScroll.destroy();
                              myScroll = null;
                              }
                              $(".news_headlines").hide();
                            loadGalleryPage(galleryArrIds[galleryPos])});
		
			if(textStatus == "success"){
				$(".imagegalleryScroller").css({width: (parseInt($("#galleryUL li").length)*378)});
				if(!imgGallery){
				          imgGallery = new iScroll('imagegallery' , { checkDOMChanges: true, snap: 'li', momentum: false, hScrollbar: false, vScroll: false,
                                   onScrollMove: function () { 
                                   $("#imgGalleryTeaser").fadeOut(); 
                                   },onBeforeScrollEnd: function(e) {
                                   e.stopPropagation();
                                   e.preventDefault();
                                   },
                                   onScrollEnd: function () {
                                   myScroll.refresh();
                                   var trueLeft = $('.imagegalleryScroller').offset().left - $('#imagegallery').offset().left;
                                   galleryPos = Math.abs(parseInt(trueLeft)/378); 
                                   },
                                   });
				}
			}
		});
		
	}
//getGallery() ends here

//setSections() starts here	
function setSections(){

 var mainIndex=0;
 var subIndex=0;
 var secID = 0;
 var parentArray = [];
 var sectionArray = [];
 var parentsectionid=0;
 var currentSelectedSection = 0;
 var currentselectedsubsection=0;
 $.getJSON( jsonBaseFeed +"sectioninformation?callback=?&city=news",function(json, textStatus){
  var dOrder = 0;
  $.each(json.data[0].section, function(j,data){  
   if(j>0){
    dOrder = dOrder + 1;
    if(data.parentId == 1) {
     mainIndex++; 
    parentsectionid=data.secId;
     parentArray.push({
      secid:data.secId
     }); 
     secID = 0;
     secID = data.secId;
     $("<li/>").attr("id","liSecMenu"+secID).attr("secId",data.secId).addClass("subNav").html(data.secName).appendTo("#TabNav").click(function(){
citySelected=0;
     	currentSelectedSection = $(this).attr("secId");

			$(this).addClass("active");
			$('.dropdown').removeClass("active");
     	$("#subSecScroller").empty();
		$("#subSecContainer").hide();

		if($(this).children().length==0)
{
$("subSecContainer").hide();
}


$("#TabNav li").each(function(){

if(currentSelectedSection == $(this).attr("secId")){}
	else{
		$(this).removeClass("active");
	}
    });
    
loadSectionScreen(data.secId);

     	var currentsecID=$(this).attr("secId");
     for(var i=0;i<sectionArray.length;i++)
     {
subIndex++;
     	if(sectionArray[i].parentsection==currentsecID)
     	{
     		$("#subSecContainer").show();
     		$("<ul/>").attr("id","sublist"+secID).appendTo("#liSecMenu"+secID).appendTo("#subSecScroller");
     		$("<li/>").attr("id","liSub"+subIndex).attr("secId",sectionArray[i].childsecID).html(sectionArray[i].childName).removeClass("subSecHighlight").appendTo("#sublist"+secID).click(function(){
citySelected=0;
currentselectedsubsection = $(this).attr("secId");
		$(this).addClass("subSecHighlight");
     	 $('.dropdown').removeClass("active");
$("#subSecScroller ul li").each(function(){
if(currentselectedsubsection==$(this).attr("secId")){}
else
{
$(this).removeClass("subSecHighlight");
}
}); 
      loadSectionScreen($(this).attr("secId"));

   }); 
}
  	}

var subsecwid=0;
var subSecScrollerwidth=$("#subSecScroller ul").outerWidth();
$("#subSecScroller ul li").each(function(){
subsecwid+=$(this).outerWidth();
});
$("#subSecScroller").width(subsecwid);
if(subSecScrollerwidth<=subsecwid){

	myNavSubScroll = "";
 myNavSubScroll = new iScroll('subSecContainer', {hideScrollbar: true, vScroll:false, checkDOMChanges: true, 
    snap: 'li', momentum:true, hScrollbar: false, bounceLock:true,  }); 


}

     });


 }
 else
 {
sectionArray.push({
	childsecID:data.secId,
	childparentID:data.parentId,
	childName:data.secName,
	parentsection:parentsectionid
});
 }
}
  });

for(var m=0;m<parentArray.length;m++){
var childfound = 0;
 for(var n=0;n<sectionArray.length;n++){
	if(sectionArray[n].parentsection == parentArray[m].secid){
		childfound = 1;
}
}
   if(childfound == 0){
$("#liSecMenu"+parentArray[m].secid).removeClass("subNav");
}
  }


  $.each(json.data[0].city, function(j,data){
   dOrder = dOrder + 1;
   $("<li/>").attr("id","liCityMenu"+data.cityId).attr("secId",data.cityId).html(data.cityName).appendTo(".dropdown").click(function(){
citySelected=1;
$("#subSecContainer").hide();
//alert($("#cityTitle"));
$("#cityTitle").text(data.cityName);
$("#TabNav li").removeClass("active");
      loadSectionScreen(data.cityId);

     });
  });
  for(var m=0;m<parentArray.length;m++){
 if(initialWidth == 0){
 	initialWidth = $("#liSecMenu"+parentArray[m].secid).outerWidth();
 }
 else{
 	initialWidth = initialWidth + $("#liSecMenu"+parentArray[m].secid).outerWidth();
 }
 	
   if($("#sublist"+parentArray[m].secid).children().length==0){
   }
  }
  NavWidthSet();
  myNavScroll = "";
  myNavScroll = new iScroll('navigation', {hideScrollbar: true, vScroll:false, checkDOMChanges: true, 
    snap: 'li', hScrollbar: false,  }); 
 });
}


//setSections() ends here
function NavWidthSet(){
   $("#navigation").width($(window).width() - $("#cities").outerWidth()); 
 $("#Navscroller").width(initialWidth);

        } // NavWidthSetend 

//getSections() starts here	
	function getSections(results){

var mainIndex=0;
var subIndex=0;
    $.each(results.rows, function(rowIndex){ 
    	   var row = results.rows.item(rowIndex);
    	
           if((row.cityFlag == 0) || (row.cityFlag == 2)){
           if(row.active != 0) {
                  mainIndex++;   // if(row.mainSection == 0){
           $("<li/>").attr("id","liSecMenu"+mainIndex).html(row.sectionName).appendTo("#TabNav");
           $("<ul/>").attr("id","sublist"+mainIndex).addClass("subNav").appendTo("liSecMenu"+mainIndex);
           //}
           }
           if(row.active == 1){
           	subIndex++;
           	$("<li/>").attr("id","liSecMenu"+subIndex).html(row.sectionName).appendTo("#sublist"+mainIndex);

 }
           } 
           else {
           
           $("<li/>").attr("id","liCityMenu"+rowIndex).html(row.sectionName).appendTo(".dropdown");
           
           }
           });

  NavWidthSet();
         myNavScroll = "";
     myNavScroll = new iScroll('navigation', {hideScrollbar: true, vScroll:false, checkDOMChanges: true, 
    snap: 'li', momentum:true, hScrollbar: false, bounceLock:true,  }); 
    }

//getSections() ends here

//getSectionMenu() starts here	
	function getSectionMenu(){
    getSection("displayOrder", getSections);
	}
//getSectionMenu() ends here

//setManageSections() starts here
function setManageSections(results){
    $("#sortable").empty();
    var mainPar = 0;
    var subPar = 0;
    var l = 0;
    getParentSection(function(results){
        var chkVar = 0;
        var subSectionId = 0;
        var subSSId = 0;
        for(var i=0;i<results.rows.length;i++){
            var row = results.rows.item(i);
            var aname = row.aname;
            var aid = row.aid;
            var aname = row.aname;
            var apid = row.apid;
            var city = row.city;
            var active = row.active;
            if((city == 0) || (city == 2)){
                
                if (l == 0) {
                    mainPar = apid;
                    subSectionId = aid;
                } else if ((mainPar == apid) && (l != 0)){
                    chkVar = 0;
                    subSectionId = aid;
                }else if((subSectionId == apid) && (l != 0)){
                    chkVar = 1;
                    subSSId = aid;
                } else if((subSSId == apid) && (l != 0)){
                    chkVar = 2;
                }
                if (chkVar == 0) {
                    $("<li/>").attr("id","liSM"+aid).appendTo("#sortable").addClass("dipOrder").attr("sid",aid);
                    var checkInput = $("<input/>").attr("id","liSM"+aid+"ID").attr("name",aname).attr("type","checkbox").attr("value","");
                     
                     $("#liSM"+aid).append(aname);
                    
                } else if (chkVar == 1) {
                    
                    $("<li/>").attr("id","liSM"+aid).appendTo("#sortable").addClass("sub-section").attr("sid",aid);
                    var checkInput = $("<input/>").attr("id","liSM"+aid+"ID").attr("name",aname).attr("type","checkbox").attr("value","");
                }
    
    else if(chkVar == 2){
                    $("<li/>").attr("id","liSM"+aid).appendTo("#sortable").addClass("sub-section1").attr("sid",aid);
                    var checkInput = $("<input/>").attr("id","liSM"+aid+"ID").attr("name",aname).attr("type","checkbox").attr("value","");
    }
                
                if(chkVar != 0) {
                   if(active != 0) {
                     checkInput.appendTo("#liSM"+aid);
                     checkInput.attr('checked','checked');
                   }else{
                     checkInput.appendTo("#liSM"+aid);
                     
                   }
                 $("#liSM"+aid).append(aname);
                }
                l++;
                
                
            }
        }
        
        
        $(".sub-section").bind('click', function() {
            var inputId = "#liSM" + $(this).attr("sid") + "ID";
            if($(inputId).is(':checked')){
                $(inputId).removeAttr("checked");
                updateSection(0, $(this).attr("sid"), '');
            } else {
                $(inputId).attr('checked','checked');
                updateSection(1, $(this).attr("sid"), '');
            }
            return false;
        });
        
        $(".sub-section1").bind('click', function() {
            var inputId = "#liSM" + $(this).attr("sid") + "ID";
            if($(inputId).is(':checked')){
                $(inputId).removeAttr("checked");
                updateSection(0, $(this).attr("sid"), '');
            } else {
                $(inputId).attr('checked','checked');
                updateSection(1, $(this).attr("sid"), '');
            }
            return false;
        });
        
        if(!manageSectionScroll){
            manageSectionScroll = new iScroll('manage_section', { checkDOMChanges: true, momentum: true, vScrollbar:false,onScrollMove: function () {tiptipvar.fadeOut() },});
        }
    });

}
//setManageSections() ends here
	
//loadGalleryImg() starts here	
function loadGalleryImg(galleryId, json){
		    for(var i=0;i<galleryImageId.length;i++){
        if(galleryImageId[i].id == galleryId){
            $("#liImg_gThumb"+galleryId).addClass("active");
        }
        else{
            if($('#liImg_gThumb'+galleryImageId[i].id).hasClass('active')){
                $("#liImg_gThumb"+galleryImageId[i].id).removeClass("active");
            }
        }
        
        $('#'+galleryImageId[i].id).unbind('click');
    }
    $("#sliderslist").empty();
    
    $("#showNumber1").text("1");
		$.each(json.data, function(j,data){	
			if(data.articleid == galleryId)	{
				$("#total").empty(); $("#total").append(data.media.length);
               
				$.each(data.media, function(k,mddata){
                       var ln = data.media.length - 1;
                       
                          if(k==0){
                         $("<li/>").attr("id","liImg_gBig"+k).appendTo("#sliderslist");
                         $("#liImg_gBig"+k).css({"visibility":"visible"});
                         }
                         else{
                         $("<li/>").attr("id","liImg_gBig"+k).appendTo("#sliderslist");
                         $("#liImg_gBig"+k).css({"visibility":"hidden"});
                         }
                       $("<div/>").addClass("figure").attr("id","figure"+k).appendTo("#liImg_gBig"+k);
                        $("<img/>").attr("id","img"+k).attr("src", mddata.image).appendTo("#figure"+k);
                         $("<div/>").addClass("figcaption").attr("id","figcaption"+k).appendTo("#figure"+k);
                         $("<h2/>").text(convertHTML(data.title)).appendTo("#figcaption"+k);
                         $("<p/>").html(convertHTML(mddata.caption)).appendTo("#figcaption"+k);

if(k==ln){
                            
                         var slider3 = new Swipe(document.getElementById('slider3'));

                         $("#slider3").delay(1000).css({"visibility":"visible"});
                         setTimeout(function(){
                         	 $("#galleryOverlay").delay(1000).fadeOut();
                                    
                                    $("#footerGallery").css({"visibility":"visible"});
                                    
                                    $('#galleryContainerOne').css({"visibility":"visible"});
                                

                                    $("#loading").hide();
                                    },2000);
           
                         
                         
                         $("#appLogo").unbind("click").bind("click",function(){isGalleryLoaded = 0;showHome();});                       
                         
                         for(var j=0;j<galleryImageId.length;j++){
                         var galleryId=galleryImageId[j].id;
                         loadGalleryFalg = 1;
                         $('#'+galleryId).unbind('click').bind("click",function(){
           
                                               loadGalleryImg(this.id,json);
                                               });
                         }
                         
                         $(".close").unbind("click").bind("click",function(){
      
                                          $(".news_headlines").show();
                                          $("#galleryContainerOne").empty().hide();
                                          $("#header").show();
                                          $("body").css({"background":"#fff"});
                           
                                          if(currentScreen == "H"){
                                          isGalleryLoaded = 0;
                                          showHome();
                                          }
                                          if(currentScreen == "S"){

                                          backScreen();
                                          }

                                          });
                         }

				});
			}
		});
	}


//loadGalleryImg() ends here

//loadGalleryPage() starts here	
	function loadGalleryPage(galleryId){
		$("#loading").css('width','0').show();
			$("#backScreen").hide();
    		if(isGalleryLoaded == 0){
        isGalleryLoaded = 1;
        activePageArray.push({
                             articleId:"",
                             sectionId:"",
                             parentArticleId:"",
                             searchKeyWrd:"",
                             currentScreen:"G"
                             });
    }

		if (myScroll) {
			$("#pullDown").hide();
			myScroll.destroy();
			myScroll = null;
		}	
		

		var GalleryThumbScroll;
    galleryImageId = [];
    galleryImageIDStuck = [];
    $("#homeContainer").hide();
    $('#articleContainer').html("").empty();
    $('#sectionContainer').html("").empty();
    $(".news_headlines").hide();
    $("#header").hide();
    $("#loading").css('width','0').show();
    $("body").css({"background":"#000"});

    
    $('#galleryContainerOne').hide().html("").fadeIn();
			$("#galleryContainerOne").css({"visibility":"hidden"});
               $("#galleryOverlay").fadeIn();

		$('#galleryContainerOne').load('gallery.html', function() {
			
                                   
                                   $("#footerGallery").css({"visibility":"hidden"});
			$.getJSON( jsonBaseFeed + "photogallery?callback=?&photo=gallerylist", function(json, textStatus){

                                    setTimeout(function(){loadGalleryImg(galleryId, json);},2000);
			
				$.each(json.data, function(j,data){
					var gId = data.articleid;

					galleryImageId.push({
                   id:gId
                   
                   });

					$("<li/>").attr("id","liImg_gThumb"+gId).appendTo("#thumbImages");
					$("<div/>").attr("id","divli"+gId).appendTo("#liImg_gThumb"+gId);

	$("<img/>").attr("src", data.thumbnail).attr("id",gId).appendTo("#divli"+gId);
$("<p/>").html(convertHTML(wrapString(data.title))).appendTo("#liImg_gThumb"+gId);
                  $(".thumbWidthSet").css({width: (parseInt($("#thumbImages li").length)*170)});

                  galleryImageIDStuck.push({
                                           
                                           id:gId
                                           
                                           });

				});
		
			});
		});
	}
	
//loadGalleryPage() ends here	
	
//loadSectionScreen() starts here

function loadSectionScreen(mySecId, fromB){

	var mainLength=0;
	var galleryLength = 0;
	var loop = 0;
	var aCount = 0;
	var myLContainer;
	var templateArray=["secTempdataCol-1","secTempdataCol-2","secTempdataCol-3","secTempdataCol-4","secTempdataCol-5"];

		if ((notificationvar.css("display") != "none")&&(subSecContainervar.css("display") != "none")) {
			headerwrapvar.css({ position: 'absolute',top:notificationvar.height()});
			wrappervar.css("top", notificationvar.height()+headerwrapvar.outerHeight());$('.multi-col').height($('article').height());

}
else if((notificationvar.css("display") == "none")&&(subSecContainervar.css("display") != "none"))
{
$('.multi-col').height($('article').height());

headerwrapvar.css({ position: 'absolute',top:0});
			wrappervar.css("top", headerwrapvar.outerHeight());
		}

		$(".notification_close").click(function(){
			notificationvar.hide();
		$(".notification_overlay").fadeOut(100);
		headerwrapvar.css("top","0px");
		wrappervar.css("top", headerwrapvar.outerHeight()+notificationvar.height());
	});

		isFromSearch = 0;

		if(fromB!="B"){
			activePageArray.push({
				articleId:"",
				sectionId:mySecId,
				parentArticleId:"",
				searchKeyWrd:"",
				currentScreen:"S"
			});
		}
		$("#galleryContainer").html("").hide();
		$("#articleContainer").html("").hide();
		homecontainervar.hide();
		 gallerycontaineronevar.empty().hide();
		$("#loading").show();
			backscreenvar.show().css("visibility", "visible").unbind("click");
    		searchvar.unbind("click");
    		$("#appLogo").unbind("click");
    		homebtnvar.show().css("visibility", "visible").unbind("click");
		pageHistory = [];
		myLContainer = "";
		if (!myScroll) {
			loaded();
		}

		currentScreen="S";
$("#sectionContainer").show().html("").css("visibility", "hidden");
		var templateURL = templateBaseFeed + 'section.html' + "?tstamp=" +(new Date()).getTime();
		$('#sectionContainer').load( templateURL, function() {

			var min = 0;
			var max = 0;
			var cityInfoLength  = 0;
			var calcul = 0;

				$.getJSON( jsonBaseFeed + "sbd?&ts=&callback=?&sid="+mySecId,function(json, textStatus){
					
galleryLength = json.data.media.length;
cityInfoLength = json.data.article.length;
if (cityInfoLength > 3){
	mainLength = 4;
}

 if((mainLength<3)&&(mainLength>0) && (galleryLength>0)){
min=1;
}
else if((galleryLength>0) && (mainLength > 2)){
min = 4;
 }else if((galleryLength>0) && (mainLength == 0)){
  min = 3;
  }
else if((galleryLength == 0) && (mainLength > 2)){
min = 6;
} else if((galleryLength == 0) && (mainLength < 3) && (mainLength > 0)){
min = 3;
}else if((galleryLength == 0) && (mainLength == 0)){
min = 5;
}
max = mainLength + min+15;
                               if(cityInfoLength < max){
                               var cityArticleDiff = cityInfoLength - mainLength - min;
                               if(cityArticleDiff < 5){
                               max = mainLength + min;
                               }
                               else{
                               var cityArtExtRow = cityArticleDiff/5;
                               max = mainLength + min + (parseInt(cityArtExtRow)*5);
                               }
                               }
					if(json.data.s == 1){
						$("#secTempgalleryUL, #secTempdataMain, #secTempdataCol-1, #secTempdataCol-2, #secTempdataCol-3, #secTempdataCol-4, #secTempdataCol-5").empty();
						$("#gHeader").unbind('click').click(function(){
													$(".news_headlines").hide();
							loadGalleryPage(galleryArrIds[galleryPos])
						});
						sectionInWords = convertHTML(json.data.sname);

						var link = 'http://wap.bonzai.mobi/req?medium=web&zone=1365&referer='+sectionInWords+'&weblink='+encodeURIComponent(location.href);
						$('#advt').show();
						$('#iFrameAd').attr('src', link).load(function(){resizeContentWindow();}).error(function(){$('#advt').hide();resizeContentWindow();});
						sectionName = sectionInWords;
if(citySelected==0)
{
$("#cityTitle").text("City News");
$("#secNameDisplay").html(sectionInWords);
$("#secNameDisplay").css("visibility","visible");
}
else
{
$("#secNameDisplay").css("visibility","hidden");
}
						
						galleryArrIds = [];
						$("#secTempimgGalleryTeaser").hide();
													

						$.each(json.data.media, function(k,data){
						//alert("k--------"+data.aid);
							$("#galleryContainer").show();
							$("#secTempimgGalleryTeaser").show();
							galleryArrIds[k] = data.aid;
							$("<li/>").attr("id","lisImg"+k).appendTo("#secTempgalleryUL");
							$("<img/>").attr("src", data.im).attr("height","200").attr("id","gsImg"+k).appendTo("#lisImg"+k);
							$("<div/>").attr("id","galleryDiv2"+k).addClass("caption").appendTo("#lisImg"+k).unbind("click").click(function(){
                            if(myScroll){
                              myScroll.destroy
                               myScroll.destroy();
                               myScroll = null;
                                }
                            $(".news_headlines").hide();
                            loadGalleryPage(galleryArrIds[galleryPos]);      
                        });

							$("<p/>").html(convertHTML(convertHTML(data.ca))).appendTo("#galleryDiv2"+k);
						});
						$(".secimagegalleryScroller").css({width: (parseInt($("#secTempgalleryUL li").length)*378)});
						
						
						imgSecGallery = new iScroll('secTempimagegallery' , { checkDOMChanges: true, snap: 'li', momentum: false, hScrollbar: false, 
							onScrollMove: function () { $("#secTempimgGalleryTeaser").fadeOut() },
							onScrollEnd: function () {
								var trueLeft = $('.secimagegalleryScroller').offset().left - $('#secTempimagegallery').offset().left
								galleryPos = Math.abs(parseInt(trueLeft)/378); 
							},
						});					
					
						var container = "secTempdataMain";
						
						$.each(json.data.article, function(j,data){
						
							if(j==json.data.article.length-1){

								$("#loading").hide();
								$('#sectionContainer').show();
								$('#sectionContainer').css("visibility", "visible");

$('article').die("click").live('click',function(event) {
				if (myScroll) {
				myScroll.destroy();
				myScroll = null;
			}		
			currentSectionId = $(this).attr("secId");
			currentArticleId = $(this).attr("artId");
			homecontainervar.hide();
			 gallerycontaineronevar.empty().hide();
			$("#articleContainer").fadeIn();
			currentScreen = "A";
			loadArticleScreen(currentSectionId, currentArticleId);
		});
	
                               homebtnvar.unbind("click").click(function(){isGalleryLoaded = 0;showHome();});
                           //   newsSectionsvar.unbind("click");
                               backscreenvar.unbind("click");
                               searchvar.unbind("click");
                               backscreenvar.bind("click",function(){
backscreenvar.css("outline","none");
backScreen();

							});
                               $("#appLogo").unbind("click").bind("click",function(){isGalleryLoaded = 0;showHome();});
                               
                               				}

                             if(j<4){
                             		mainLength++;
                             	//============ Display main section articles ===============

                             		if(j==0){
                             			//alert("sid = "+json.data.article[j].sid);
                             			
								$(".main_article").show();
								$("<article/>").attr("id","main"+j).attr("secId",mySecId).attr("artId",data.aid).appendTo("#"+container);
								$("<div>").addClass("art_content").attr("id","mainDiv"+j).appendTo("#main"+j);
								if(data.hi == 1){
									$.each(data.me, function(h,data){
								if(h==0)
										{
										$("<img/>").attr("src", data.im).attr("width","200").appendTo("#mainDiv"+j);

										}
									});
								}
								
								$("<h2/>").html(convertHTML(data.ti)).appendTo("#mainDiv"+j);
							} else {
								$("<article/>").attr("id","main"+j).attr("secId",mySecId).attr("artId",data.aid).appendTo("#"+container);
								$("<div>").addClass("art_content").attr("id","mainDiv"+j).appendTo("#main"+j);
								if(data.hi == 1){
									$.each(data.me, function(h,data){
										if(h==0){
										$("<img/>").attr("src", data.im).attr("width","100").appendTo("#mainDiv"+j);
										}
									});
								}
								
								$("<h2/>").html(convertHTML(data.ti)).appendTo("#mainDiv"+j);
							}
							$("<time/>").text(data.pd).appendTo("#mainDiv"+j);
							$("<p/>").html(convertHTML(data.de)).appendTo("#mainDiv"+j);

                             	//============ Display main section articles ===============


                             } 
                             else
                             { 

if (max > j) {
                             	//============= askjskfsakfjsdf ===========


                             	if((mainLength>1) && (galleryLength>0) && (loop==0)){ 
                               aCount=4; 
                               
                               loop++;
                               
                               }else if((mainLength>2) && (galleryLength==0) && (loop==0)){
                               
                               aCount=2;
                               
                               loop++;
                               
                               
                               }else if((mainLength) && (mainLength>0) && (galleryLength==0) && (loop==0)){
                               
                               aCount=2;
                               
                               loop++;
                               
                               
                               }else if((mainLength==0) && (galleryLength>0) && (loop==0)){
                               
                               // alert("sfghjdh");
                               aCount=0;
                               
                               loop++;
                               
                               }else if((mainLength==0) && (galleryLength==0) && (loop==0)){
                               
                               
                               aCount=0;
                               
                               loop++;
                               
                               
                               }
                               
                               
                               
                               
                               if((aCount>4) && (mainLength>1) && (galleryLength>0) && (loop<2)){
                               
                               aCount=2;
                               
                               loop++;
                               
                               }
                               
                               else if((aCount>4) && (mainLength) && (galleryLength>0) && (loop<2)){
                               
                               aCount=0;
                               
                               loop++;
                               
                               }else if((aCount==2) && (mainLength==0) && (galleryLength>0) && (loop<2)){
                               
                               aCount=4;                                                   
                               
                               }else if((aCount>4) && (mainLength==0) && (galleryLength>0) && (loop<2)){
                               //alert("222");
                               aCount=0;  
                               
                               loop++;
                               
                               }
                               
                               else if((aCount>4) && (mainLength>2) && (galleryLength==0) && (loop<2)){
                               
                               aCount=2;
                               
                               loop++;
                               
                               
                               } else if((aCount>4) && (mainLength && mainLength>0) && (galleryLength==0) && (loop<2)){
                               
                               aCount=2;
                               
                               loop++;
                               
                               
                               } 
                               
                               else if((aCount>4) && (loop>2)){
                               // alert("222-----");
                               
                               aCount=0;
                               
                               
                               }else if(aCount>4){
                               //  alert("222");
                               
                               aCount=0;
                               
                               
                               }
                               //alert(aCount+" Imag = 1 "+ mainLength);
                               
                               myLContainer = templateArray[aCount];
                               
                               aCount++;

                             	//=========================================


						

                             $("<article/>").attr("id","secId"+j).attr("secId",mySecId).attr("artId",data.aid).appendTo("#"+myLContainer);
							$("<div>").addClass("art_content").attr("id","secDiv"+j).appendTo("#secId"+j);
							$("<h3/>").html(data.sname).appendTo("#secDiv"+j);
							if(data.hi == 1){
									$.each(data.me, function(h,data){
										if(h==0){
										$("<img/>").attr("src", data.im).attr("width","155").appendTo("#secDiv"+j).load(function(){ myScroll.refresh(); });
										}
									});
								}
							$("<h2/>").html(convertHTML(data.ti)).appendTo("#secDiv"+j);
							$("<time/>").text(data.pd).appendTo("#secDiv"+j);
							$("<p/>").html(convertHTML(data.de)).appendTo("#secDiv"+j);
}
                             }
							
						});
						
						
						if(textStatus == "success"){
							myScroll.refresh();
						}
						
					}
					else {
						$("#secTempdataMain").html("No Data Currently Available for this Section...");
					}
					
				});
			});
			
	}


//loadSectionScreen() ends here

// loadArticleScreen() starts here	
function loadArticleScreen(mySecID, myArtID, fromB){
if(notificationvar.css("display")!="none")
{
headerwrapvar.css("top",notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+headerwrapvar.outerHeight());
$('.article-content').height($(window).height()-(notificationvar.outerHeight()+headerwrapvar.outerHeight()));
}
else 
{
headerwrapvar.css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
}

$(".notification_close").click(function(){
			notificationvar.hide();
		$(".notification_overlay").fadeOut(100);
		headerwrapvar.css("top","0px");
		wrappervar.css("top", headerwrapvar.outerHeight());
		$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
	});

myArticleLead = [];

$("#sectionContainer").empty().html("").hide();
			if(isFromSearch == 0){
		if((currentScreen == "R") && (fromB!="B")){
		activePageArray.push({
                             articleId:myArtID,
                             sectionId:mySecID,
                             parentArticleId:"",
                             searchKeyWrd:"",
                             currentScreen:"R"
                             });		
		
		}
		
		if((currentScreen == "A") && (fromB!="B")){

		activePageArray.push({
                             articleId:myArtID,
                             sectionId:mySecID,
                             parentArticleId:"",
                             searchKeyWrd:"",
                             currentScreen:"A"
                             });		
		}

	backscreenvar.show().css("visibility", "visible").unbind("click");
	
}
	
    	//	newsSectionsvar.bind("click");
    		searchvar.unbind("click");
    		$("#appLogo").unbind("click");
    		homebtnvar.css("visibility", "visible").unbind("click");
$('article').die("click");
		$("#loading").css('width','0').show();
		fromB = fromB || "N"
		if(fromB == "N"){
			pageHistory.push("loadArticleScreen('"+mySecID+"','"+myArtID+"','Y')");
		}
		if(currentScreen == "H"){
			currentScreen = "S";
		} else {
			currentScreen = "A";
		}

$('#articleContainer').html("");
	$('#articleContainer').css("visibility", "hidden");
		$('#articleContainer').load('article.html', function() {
						myArticleData = [];
				myArticleJson = [];
				myTitleData = [];
				myArticleImages = [];
				myArticleImageCaption = [];
				myArticlePubDate = [];
				myArticleByLine = [];
				myArticleRelnewsTitle = [];
				myArticleWebURL = [];
				myArticleComment = [];
				
				var newUrl = jsonBaseFeed + "sbd?callback=?&sid="+mySecID+"&ts=";
				$.getJSON(newUrl,function(json, textStatus){
					sectionInWords = convertHTML(json.data.sname);
					$("#artSection, #sectionNameDisp").html(sectionInWords);
					navtabvar.empty();
					 $.each(json.data.article, function(j,data){
						
						var myArticleId = data.aid
						myArticleWebURL[myArticleId] = data.al; 
						myArticleData[myArticleId] = data.de;
					
						myTitleData[myArticleId] = data.ti;
						myArticlePubDate[myArticleId] = data.pd;
						myArticleByLine[myArticleId] = data.au;
						myArticleComment[myArticleId] = data.cmt;
						myArticleImages[myArticleId] = [""];
						myArticleImageCaption[myArticleId] = [""];
						myArticleRelnewsTitle[myArticleId] = [""];
						$.each(data.me, function(k,media){
							myArticleImageCaption[myArticleId][k] = media.ca;
							myArticleImages[myArticleId][k] = media.im;
						});
							$.each(data.rn, function(k,relnews){
							if(k>0) separator = "<div class='b_border'></div>"; else separator = "";
                                   var span1 = "<span class='relNews'  mainArtId ='"+myArticleId+"' artid='"+ relnews.aid +"'>" + separator + convertHTML(relnews.ti) + "</span>";
							myArticleRelnewsTitle[myArticleId] = myArticleRelnewsTitle[myArticleId]  + "<span class='relNews'  mainArtId ='"+myArticleId+"' artid='"+ relnews.aid +"'>" + separator + convertHTML(relnews.ti) + "</span>";
						});
				

						if(data.ti.length < 35){

							$("<li/>").attr("id",data.aid).addClass("articleHeadline").html(convertHTML(data.ti)+"<time>"+data.pd+"</time>").appendTo("#navTab");	
						}
						else{
							$("<li/>").attr("id",data.aid).addClass("articleHeadline").html(convertHTML(data.ti.substring(0,35)+"...")+"<time>"+data.pd+"</time>").appendTo("#navTab");	
						}

	
					});
					openArticle(myArtID,sectionInWords);
					$('#headWidthSet').css({width: (parseInt($("li.articleHeadline").length)*250)}); 
tabScroll = new iScroll('headWrapper', {
		checkDOMChanges: true, 
						snap: 'li',
						momentum: true,
						vScroll:false,
						hScrollbar: false,
						bounceLock:true,
		onScrollEnd: function () {
			document.querySelector('#indicator > li.active').className = '';
			document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
		}
	});

					 articleScroll = new iScroll('artWrapper', { checkDOMChanges: true, hScrollbar: false, vScroll:false });	
				});
			});
	}



function loadRelatedArticle(mySecID, mainArtId,artId, fromB){
   
if(notificationvar.css("display")!="none")
{
headerwrapvar.css("top",notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+headerwrapvar.outerHeight());
$('.article-content').height($(window).height()-(notificationvar.outerHeight()+headerwrapvar.outerHeight()));
}
else
{
headerwrapvar.css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
}
    
    myArticleLead = [];
    
    $("#sectionContainer").empty().html("").hide();
    if(isFromSearch == 0){
		if((currentScreen == "R") && (fromB!="B")){
            activePageArray.push({
                                 articleId:artId,
                                 sectionId:mySecID,
                                 parentArticleId:"",
                                 searchKeyWrd:"",
                                 currentScreen:"R"
                                 });
            
		}
		
		if((currentScreen == "A") && (fromB!="B")){
            
            activePageArray.push({
                                 articleId:myArtID,
                                 sectionId:mySecID,
                                 parentArticleId:"",
                                 searchKeyWrd:"",
                                 currentScreen:"A"
                                 });
		}
        
        backscreenvar.show().css("visibility", "visible").unbind("click");
        
    }
	
  //  newsSectionsvar.bind("click");
    searchvar.unbind("click");
    $("#appLogo").unbind("click");
    homebtnvar.css("visibility", "visible").unbind("click");
    $('article').die("click");
    $("#loading").css('width','0').show();
    fromB = fromB || "N"
    if(fromB == "N"){
        pageHistory.push("loadArticleScreen('"+mySecID+"','"+artId+"','Y')");
    }
    if(currentScreen == "H"){
        currentScreen = "S";
    } else {
        currentScreen = "A";
    }
    
    $('#articleContainer').html("");
	$('#articleContainer').css("visibility", "hidden");
    $('#articleContainer').load('article.html', function() {
                                myArticleData = [];
                                myArticleJson = [];
                                myTitleData = [];
                                myArticleImages = [];
                                myArticleImageCaption = [];
                                myArticlePubDate = [];
                                myArticleByLine = [];
                                myArticleRelnewsTitle = [];
                                myArticleWebURL = [];
                                                                
                                var newUrl = jsonBaseFeed + "sectionarticle?callback=?&sectionid="+mySecID+"&articleid="+mainArtId;
            
                                $.getJSON( jsonBaseFeed + "sectionarticle?callback=?&sectionid="+mySecID+"&articleid="+mainArtId,function(json, textStatus){
                                          
                                          sectionInWords = convertHTML(json.data[0].sectionName);
                                          $("#artSection, #sectionNameDisp").html(sectionInWords);
                                          
                                          $("#navTab").empty();
                                          $.each(json.data[0].article, function(j,data){
                                                 
                                                 var myArticleId = data.articleId
                                                 myArticleWebURL[myArticleId] = data.articlelink;
                                                 myArticleData[myArticleId] = data.description;
                                                 myTitleData[myArticleId] = data.title;
                                                 myArticlePubDate[myArticleId] = data.pubDate;
                                                 myArticleByLine[myArticleId] = data.author;
                                                 myArticleImages[myArticleId] = [""];
                                                 myArticleImageCaption[myArticleId] = [""];
                                                 myArticleRelnewsTitle[myArticleId] = [""];
                                                 
                                                 //get images
                                                 $.each(data.media, function(k,media){
                                                        myArticleImageCaption[myArticleId][k] = media.caption;
                                                        myArticleImages[myArticleId][k] = media.image;
                                                        //alert(k + "-" +media.thumbnail);
                                                        });
                                                 //to get related news
                                                 $.each(data.relnews, function(k,relnews){
                                                        if(k>0) separator = "<div class='b_border'></div>"; else separator = ""; // This section modified by raghav
                                                        myArticleRelnewsTitle[myArticleId] = myArticleRelnewsTitle[myArticleId]  + "<span class='relNews' mainArtId='"+myArticleId+"' artid='"+ relnews.articleId +"'>" + separator + convertHTML(relnews.title) + "</span>";
                                                        });
                                                 
                                                 
                                                 if(data.title.length < 35){
                                                 
                                                 $("<li/>").attr("id",data.articleId).addClass("articleHeadline").html(convertHTML(data.title)+"<time>"+data.pubDate+"</time>").appendTo("#navTab");	
                                                 }
                                                 else{
                                                 $("<li/>").attr("id",data.articleId).addClass("articleHeadline").html(convertHTML(data.title.substring(0,35)+"...")+"<time>"+data.pubDate+"</time>").appendTo("#navTab");	
                                                 }
                                                 
                                                 
                                                 });
                                          navtabvar.empty();
                                          openArticle(artId,sectionInWords);
                                          $('#headWidthSet').css({width: (parseInt($("li.articleHeadline").length)*250)}); 
                                          tabScroll = new iScroll('headWrapper', {
                                                                  checkDOMChanges: true, 
                                                                  snap: 'li',
                                                                  momentum: true,
                                                                  vScroll:false,
                                                                  hScrollbar: false,
                                                                  bounceLock:true,
                                                                  onScrollEnd: function () {
                                                                  document.querySelector('#indicator > li.active').className = '';
                                                                  document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
                                                                  }
                                                                  });
                                          
                                          articleScroll = new iScroll('artWrapper', { checkDOMChanges: true, hScrollbar: false, vScroll:false });	
                                          });
                                });
}
	// loadArticleScreen() ends here

	//alertMe() starts here
	function alertMe(){
		alert("C");
		$("#scroller").style("-webkit-transform", "translate3d(0px, -111px, 0px) scale(1)");

	}
	//alrtMe() ends here

	//refreshSavedArticles() starts here
	function refreshSavedArtciels(results){
		savedArticles = [];
		$("#showSavedArticle").css({ opacity: 0.5 });
		 $.each(results.rows, function(rowIndex){
			var row = results.rows.item( rowIndex );
			savedArticles[rowIndex] = row.articleId;
			$("#showSavedArticle").css({ opacity: 1 });
		});
		 		 if(savedArticles.length == 0){
			$(".controls").fadeOut();
		 	$("#artHeadline").html('<span class="timeH">No data is currently available in stored article</span>');
		 }
	}
//refreshSavedArticles() ends here

//loadSavedArticles() starts here
	function loadSavedArticle(results){
			backscreenvar.unbind("click");
    		searchvar.unbind("click");
    		$("#appLogo").unbind("click");
		backscreenvar.css("visibility", "visible");
		$('#articleContainer').html("").css("visibility", "hidden");;
		$('#articleContainer').load( templateBaseFeed + 'savedarticle.html', function() {
				var myArtID;
				myArticleData = [];
				myTitleData = [];
				myArticleImages = [];
				myArticleByLine = [];
				myArticlePubDate = [];
				myArticleRelnewsTitle = [];
				myArticleImageCaption = [];
				myArticleWebURL = [];
					$("#artSection").html("Saved Articles");
					$("#removeSaved").click(function(event) {	
						var r = confirm("Do you want to delete this Article?");
						if (r==true) {
									deleteArticle(currentArticleId , function(artId){ 
									showSavedArticles();
									getArticle( refreshSavedArtciels );
								});
								deleteImages(currentArticleId,"");
						  }							
					});
					
					 $.each(results.rows, function(rowIndex){
						var row = results.rows.item( rowIndex );
						if(rowIndex==0){
							myArtID = row.articleId
						}

						var myArticleId = row.articleId
						myArticleData[myArticleId] = row.description;
						myTitleData[myArticleId] = row.title;
						myArticlePubDate[myArticleId] = row.pubDate;
						myArticleByLine[myArticleId] = row.author;
						myArticleImages[row.articleId] = [""];
						myArticleImageCaption[row.articleId] = [""];
						myArticleRelnewsTitle[myArticleId] = [""];
						myArticleWebURL[myArticleId] = [""];
						$("<li/>").attr("id",row.articleId).html(convertHTML(row.title)+"<time>"+row.pubDate+"</time>").appendTo("#navTab");	
					});
					 getImages(getSavedData,myArtID);
					if(currentScreen == "H") currentScreen = "S";
					else currentScreen == "A";
					$("#headWidthSet").css({width: (parseInt($("li.articleHeadline").length)*250)})
					tabScroll = new iScroll('headWrapper', {
						checkDOMChanges: true, 
						snap: 'li',
						momentum: true,
						hScrollbar: false,
					});
					 articleScroll = new iScroll('artWrapper', { checkDOMChanges: true, hScrollbar: false, vScroll:false });

			});
	}
//loadSavedArticles() ends here

//postFeed() starts here
	 function postToFeed(sharingLink) {
        // calling the API ...
        var obj = {
          method: 'feed',
          link: sharingLink,
          redirect_uri: 'http://facebook.com/connect/login_success.html',
        };
//postFeed() ends here

//callback() starts here
        function callback(response) {
          document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
        }

        FB.ui(obj, callback);
      }
//callback() ends here

//openArticle() starts here
	function openArticle(currArtId,sectionName){
var plen=0;
$("#comments_tab").hide();
$("#commentCount").css("opacity",1.0);
$("#Commentslist").html(convertHTML(myArticleComment[currArtId]));
var plen = $("#Commentslist").find("p").length;
$("#commentCount").empty();
if(plen==0){
$("#commentCount").html(" 0 ").css("opacity",0.5);
$(".comments").unbind("click");
}
else{
$(plen).appendTo("#commentCount");
$(".comments").click(function(){
//alert("inside the click");
$("#comments_tab").hide();
if($("#comments_tab").css("display")=="none")
{
//alert("inside the condition");
$("#comments_tab").show();
    $("#commentsOverlay").show().click(function(){
                                                                      

                 $("#commentsOverlay").hide();
                 $("#comments_tab").hide();
                             });
}
else
{
//alert("inside the else conditions");
$("#comments_tab").hide();
}
});
}


myCommentsScroll = "";
 myCommentsScroll = new iScroll('comments', {hideScrollbar: true, hScroll:false, checkDOMChanges: true, 
   hScrollbar: false, vScrollbar: false,}); 



if(notificationvar.css("display")!="none")
{
headerwrapvar.css("top",notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+headerwrapvar.outerHeight());
$('.article-content').height($(window).height()-(notificationvar.outerHeight()+headerwrapvar.outerHeight()));
$('.multi-col').height($('article').height());
}
else
{
headerwrapvar.css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
$('.multi-col').height($('article').height());
}

$(".notification_close").click(function(){
			notificationvar.hide();
		$(".notification_overlay").fadeOut(100);
		headerwrapvar.css("top","0px");
		wrappervar.css("top", headerwrapvar.outerHeight());
		$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
	});

		if(sectionName !=null){
		var link = 'http://wap.bonzai.mobi/req?medium=web&zone=1365&referer='+sectionName+'&weblink='+encodeURIComponent(location.href);
		$('#advt').hide();
		$('#iFrameAd').attr('src', link).error(function(){$('#ArtAdvt').hide();}).load(function(){

			resizeContentWindow();
		});
		}
		
		if(articleScroll) articleScroll.scrollTo(0, 0, 200);
$('.share').unbind("click");
		var twitterA = "https://twitter.com/share?url="  + myArticleWebURL[currArtId] + "&text="+myTitleData[currArtId]+ " - &via=The Hindu";	
		$('#twitterA').click( function(){
				$('#socialShare').toggle();
			});

		$('#fbsharebutton').click(function(){
			$('#socialShare').toggle();


postToFeed(myArticleWebURL[currArtId]);
			
		});
		
		$("#twitterA").attr("href",twitterA);
		currentArticleId = currArtId;
		$('.articleHeadline').removeClass("active");
		$("#"+currArtId).addClass("active");
		var alData="";
    if((myArticleLead[currArtId]!=null) && (myArticleLead[currArtId]!="")){
        alData = '<p class="articleLead">'+myArticleLead[currArtId]+'</p>'; 
    }
    	if(alData != null){
		$("#artData").html(alData + convertHTML(myArticleData[currArtId]));
		}
		else{
		$("#artData").html(convertHTML(myArticleData[currArtId]));	
		}
		$("#myArticleByLine").html(convertHTML(myArticleByLine[currArtId])); 
		if(jQuery.inArray(currArtId, savedArticles) != "-1"){
			$("#moveToSaved").css({ opacity: 0.5 }).unbind('click');
		} else {
			$("#moveToSaved").css({ opacity: 1 }).bind('click', moveToSaved );
		}
		$("#aThumb").empty();
		$("#aGalleryTeaser").hide();
		for(j=0; j < myArticleImages[currArtId].length; j++){
			if(j==1) $("#aGalleryTeaser").fadeIn();
			var orgP = myArticleImages[currArtId][j];

			$("<li/>").attr("id","aThumb"+j).appendTo("#aThumb");
			$("<img/>").attr("src", orgP).attr("id","gBImg"+j).appendTo("#aThumb"+j).load(function(){
			
			});
			$("<p/>").html(convertHTML(myArticleImageCaption[currArtId][j])).addClass("caption").appendTo("#aThumb"+j);
		}
		$(".scrollerA").css({width: (parseInt($("#aThumb li").length)*378)});
						
		imgArtGallery = new iScroll('ascroller' , { checkDOMChanges: true, snap: 'li', momentum: false, vScroll: false, hScrollbar: false, 
			onScrollMove: function () {
				 $("#aGalleryTeaser").fadeOut("fast") 
			},
		});			
		
		$("#emailTo").unbind("click").click(function(){
							 
			var emailBody = "I thought you might be interested: \n\n" + myArticleWebURL[currArtId];
			parent.location = "mailto:?subject=" + myTitleData[currArtId] + "&body=" + encodeURIComponent(emailBody);
			$('#socialShare').toggle();
			return false
		});
		
		
		if(myArticleImages[currArtId][0] != ""){
			$("<br/>").prependTo("#artData");
			$("<div/>").addClass('img').attr("id","pImg"+currArtId).prependTo("#artData");
			$("<img/>").attr("id","artImg").attr("src", myArticleImages[currArtId][0]).appendTo("#pImg"+currArtId).click(function(){
				$("#articleImages").fadeIn();
			}).load(function(){
if(notificationvar.css("display")!="none")
{
headerwrapvar.css("top",notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+headerwrapvar.outerHeight());
$('.article-content').height($(window).height()-(notificationvar.outerHeight()+headerwrapvar.outerHeight()));
}
else
{
headerwrapvar.css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
}
				resizeContentWindow();
			});
			$("<span><img src='images/icons/zoom.svg' alt='Zoom' /></span>").addClass("zoom").appendTo("#pImg"+currArtId).click(function(){
				$("#articleImages").fadeIn();
			});
		}
		$("#artHeadline").html("<h2>"+convertHTML(myTitleData[currArtId])+"</h2>"+'<time> - '+myArticlePubDate[currArtId]+'</time>');
	
		if(myArticleRelnewsTitle[currArtId] != ""){
			$("<div/>").addClass("keywords_related").html("<strong>Related: </strong>"+myArticleRelnewsTitle[currArtId]).appendTo("#artData"); 
		}

		$('.share').unbind("click").bind("click",function(){

		setTimeout(function(){
	$("#socialShare").fadeOut();},
10000);
			 hideAllWindow();
			if ($('#socialShare').css("display") == "none") {

					$('#socialShare').show();
			}
			else{


					$('#socialShare').hide();
			}

			});
    setTimeout(function(){ 
               $('#articleContainer').css("visibility", "visible");
               $("#loading").hide();
               if(isFromSearch == 0){  	
    }
            							backscreenvar.unbind("click").bind("click",function(){
backscreenvar.css("outline","none");
backScreen();

							});
							  homebtnvar.unbind("click").click(function(){showHome();});
                               $("#appLogo").unbind("click").bind("click",function(){isGalleryLoaded = 0;showHome();});
                               
               },1200);
	}
//openArticle() ends here

//showSavedArticles() starts here
	function showSavedArticles(){
		if(savedArticles.length > 0) {
			if (myScroll) {
				myScroll.destroy();
				myScroll = null;
			}	
			getArticle( loadSavedArticle );
			homecontainervar.hide();
			 gallerycontaineronevar.empty().hide();
			$("#articleContainer").fadeIn();
		}
	}
//showSavedArticles() ends here

//moveToSaved() starts here	
	function moveToSaved(){
		saveArticle(currentArticleId, myTitleData[currentArticleId],myArticlePubDate[currentArticleId],myArticleData[currentArticleId],myArticleByLine[currentArticleId],
					function(){	
						$("#moveToSaved").css({ opacity: 0.5 }).unbind('click');
					}
		);	
				$.getJSON( jsonBaseFeed + "base64imagedeliver?callback=?&photo=base64&url=" + myArticleImages[currentArticleId][0] ,
		function(json, myStatus){
		  saveImages(currentArticleId, myArticleImageCaption[currentArticleId][0], json.data[0].image, function() {
			});
		});
		
		getArticle( refreshSavedArtciels );	
	}
//moveToSaved() ends here

//getSavedData() starts here
	function getSavedData(results,artId){
		if (!results){
			return;
		}
		$.each(
			results.rows,
			function( rowIndex ){
				var row = results.rows.item( rowIndex );
				myArticleImages[row.articleId] = [""];
				myArticleImageCaption[row.articleId] = [""];
				myArticleImageCaption[row.articleId][0] = row.caption;
				myArticleImages[row.articleId][0] = "data:image/png;base64," + row.image;
			}
		);
		openArticle(artId);
	}
//getSavedData() ends here

//resizeContentWindow() starts here
function resizeContentWindow() {

$(".notification_close").click(function(){
			notificationvar.hide();
		$(".notification_overlay").fadeOut(100);
		headerwrapvar.css("top","0px");
		wrappervar.css("top", headerwrapvar.outerHeight());
		$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
	});

if(notificationvar.css("display")!="none")
{
$('.multi-col').height($('article').height());
headerwrapvar.css("top",notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+headerwrapvar.outerHeight());
$('.article-content').height($(window).height()-(notificationvar.outerHeight()+headerwrapvar.outerHeight()));
}
else
{
$('.multi-col').height($('article').height());
headerwrapvar.css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
$(".article-content").height($(window).height()-(headerwrapvar.outerHeight()));
}

var $visibleWidth = "";
        var $x="";
			if($('footer#advt').css('display') == 'none'){
			wrappervar.css('bottom','0');
			
			}
			else{
				wrappervar.css('bottom','90px');
			myScroll.refresh();
			}
		var $visibleWidth = $("#artWrapper").width()-24;
		if($(window).height() > $(window).width()) {
			$("#articleImages").css({
				height:($(window).height()),
				width:($(window).width())
			});
	        $("#artWidthSet").width($(window).width()-24);
		} else {
			$("#articleImages").css({
				height:($(window).height()),
				width:($(window).width())
			});
			$("#artWidthSet").width($(window).width()-24);
		}
		var $x = document.getElementById("artData").scrollWidth;
		$x = parseInt($x);
		if ($x > $visibleWidth) {
			$("#artWidthSet").css("width", $x+"px");
		} else {
			$("#artWidthSet").css("width", $visibleWidth+"px");
				}
		
		}
		//resizeContentWindow() ends here
	
//wrapString() starts here	
function wrapString(text, maxchars) {
   
             if (maxchars == undefined) {
                 maxchars = 200;
           }
    
            if (text.length <= maxchars) return text;
           var finalPos = maxchars-1;
             var regex = new RegExp('[^a-z0-9]','i');
             while (1) {
                   var x = text.substring(finalPos, finalPos+1);
                 if (regex.test(x)) {
                         return text.substring(0,finalPos);
                     }
        
                   finalPos = finalPos + 1;
                  if (finalPos == text.length) return text;
                }
           }
           //wrapString ends here

$(document).ready(function(){ 

if((notificationvar.css("display"))!="none")
{
headerwrapvar.css("top",+notificationvar.outerHeight());
wrappervar.css("top",notificationvar.outerHeight()+88);
}
else
{
$("#header-wrap").css("top",0);
wrappervar.css("top",headerwrapvar.outerHeight());
}

$(".notification_close").click(function(){
			notificationvar.hide();
		$(".notification_overlay").fadeOut(100);
		headerwrapvar.css("top","0px");
		wrappervar.css("top", headerwrapvar.outerHeight());
	});



    	  
	$('#About_us_ninestars').click(function(){
		window.open('http://www.ninestars.in','_blank');
	});
	

	$('#ascroller').css('overflow','');
		Spinner({radius: 10, length: 12, lines: 8, width: 4,color: '#0968b7',}).spin(document.getElementById('loaderBig'));
	
	$(window).resize( function(){
NavWidthSet();

 resizeContentWindow();
	});

	$('body').show();
	// headerwrapvar.show();
	enable_bnews_click = 0;
	getInitData();
	 overlayDivvar.height($(window).height()).click(function(){
	 	hideAllWindow();
	 });
	tabvar.css("visibility","visible").hide();	
	$('ul.tabs').each(function(){
		var $active, $content, $links = $(this).find('a');
		$active = $links.first().addClass('active');
		$content = $($active.attr('href'));
		$links.not(':first').each(function () {
			$($(this).attr('href')).hide();
		});
		$(this).on('click', 'a', function(e){
			$active.removeClass('active');
			$content.hide();
			tiptipvar.fadeOut();
			$active = $(this);
			$content = $($(this).attr('href'));
			$active.addClass('active');
			$content.show();
			e.preventDefault();
		});
	});
	$(".settings").click(function(){

	$("settings").css("-webkit-tap-highlight-color","transparent","outline","none");
		if(tabvar.css('display') == 'none'){ 
			hideAllWindow();
		   tabvar.show();		
			 overlayDivvar.show();
		   return false;
		} else { 
			$("#tiptip_holder").fadeOut();
		   tabvar.hide(); 
		}
	});
		//newsSections();
	$('body, .sectionMenu').click(function() { 
		hideAllWindow();
	});
	$("#gHeader").click(function(){
		loadGalleryPage(galleryArrIds[galleryPos])});
		$("#tabs, .search_box").click(function(){
			return false;
		});
		$("#articleContainer").hide();
		$('.articleHeadline').live('click',function(event) {
			openArticle(this.id, sectionInWords);
		});
		$('article').live('click',function(event) {
			if (myScroll) {
				myScroll.destroy();
				myScroll = null;
			}		
			currentSectionId = $(this).attr("secId");
			currentArticleId = $(this).attr("artId");
			homecontainervar.hide();
			 gallerycontaineronevar.empty().hide();
			$("#articleContainer").fadeIn();
			currentScreen = "A";
			loadArticleScreen(currentSectionId, currentArticleId);
		});
		$('.sectionMenu').live('click',function(event) {
			currentScreen = "S";
			event.preventDefault();
			currentSectionId = $(this).attr('id');
			$("#sectionNameDisp").html($(this).html());
			homecontainervar.hide();
			$("#articleContainer").fadeOut();
			//searchcontainervar.hide().html("");
			if(myScroll){
				myScroll.destroy();
                myScroll = null;			
            }
			isGalleryLoaded = 0;
			loadSectionScreen(currentSectionId);
		});
		
		$(".relNews").live('click',function(event) {
			if(currentScreen=="A"){
				prevSectionId = currentSectionId;
				prevArticleId = currentArticleId;
			}
			currentScreen = "R";
                          
        loadRelatedArticle(currentSectionId, $(this).attr("mainArtId"),$(this).attr("artid"),"");
			
		});	
		$("#showSavedArticle").click(function(){
			showSavedArticles();
		});

		$("#articleImages").click(function(){ $(this).fadeOut(); });
	$(window).resize( function(){
		if($(window).height() > $(window).width()) {
                      var count = 0; 
                     if(currentScreen=="H"){
                     var noOfFifthcolumn = actDataColHome.length;
                     var noofRows = 0;
                     var extraArticle = 0;
                     if(count > 4){
                     noofRows = noOfFifthcolumn/4;
                     var noOfrowArticle = parseInt(noofRows)*4;
                     extraArticle = noOfFifthcolumn - noOfrowArticle;
                     }
                     else{
                     if(count < 2){
                     extraArticle = 2;
                     }
                     }
                     for(i=0;i<(actDataColHome.length - extraArticle);i++){
                     count++;
                     if(count>4){
                     
                     count = 1;
                     }
                     else{
                     $("#"+actDataColHome[i].secId).appendTo("#dataCol-"+count);
                     }
                     }
                     }
			$(".imagegalleryScrollerBig").css({width: (parseInt($("#gBig li").length)*670)});
			$(".scrollerG").css({width: (parseInt($("#gThumb li").length)*130)});
		}
		else {
                     if(currentScreen=="H"){
                     var count =0;
                     for(k=0;k<actDataColHome.length;k++){
                     count++;
                     if(count>4){
                     count = 1;
                     $("#dataCol-"+count).remove("#"+actDataColHome[k].secId);
                     }
                     else{
                     $("#dataCol-"+count).remove("#"+actDataColHome[k].secId);
                     }
                     }
                     for(i=0;i<actDataColHome.length;i++){
                     $("#"+actDataColHome[i].secId).appendTo("#dataCol-5");
                     }
                     }
			$(".imagegalleryScrollerBig").css({width: (parseInt($("#gBig li").length)*670)});
			$(".scrollerG").css({width: "auto"});
		}
		
		if(currentScreen=="A"){
			resizeContentWindow();
		}
		 overlayDivvar.height($(window).height());
	});

});