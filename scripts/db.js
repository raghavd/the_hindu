		var databaseOptions = {
			fileName: "sqlite_hd",
			version: "1.0",
			displayName: "SQLite Hindu Database",
			maxSize: 50*1024*1024
		};
 
		var database = openDatabase(
			databaseOptions.fileName,
			databaseOptions.version,
			databaseOptions.displayName,
			databaseOptions.maxSize
		);

		database.transaction(

function(transaction){

transaction.executeSql(

		"drop table section;"

	);

}

	);

		database.transaction(
			function( transaction ){
				transaction.executeSql(
					"CREATE TABLE IF NOT EXISTS articles (" +
						"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
						"articleId TEXT NOT NULL UNIQUE," +
						"pubDate TEXT NOT NULL," +
						"title TEXT NOT NULL," +
						"author TEXT NOT NULL," +
						"description TEXT NOT NULL" +
					");"
				);
			}
		);
		
		database.transaction(
			function( transaction ){
				transaction.executeSql(
					"CREATE TABLE IF NOT EXISTS images (" +
						"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
						"articleId TEXT NOT NULL," +
						"caption TEXT," +
						"image BLOB" +
					");"
				);
			}
		);
		
		database.transaction(
			function( transaction ){
				transaction.executeSql(
					"CREATE TABLE IF NOT EXISTS section (" +
                                           "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                           "sectionId INTEGER NOT NULL UNIQUE," +
                                           "displayOrder INTEGER NOT NULL," +
                                           "sectionName TEXT," +
                                           "cityFlag INTEGER," +
                                           "mainSection INTEGER," +
                                           "active INTEGER," +
                                           "parentId INTEGER NOT NULL" +
                                           ");"
				);
			}
		);
		

		var saveArticle = function( myId, myTitle, myPubData, myArtData, myAuthor, callback ){
			database.transaction(
				function( transaction ){

					transaction.executeSql(
						(
							"REPLACE INTO articles (" +
								"articleId, pubDate, title, author, description" +
							" ) VALUES ( " +
								"?,?,?,?,? " +
							");"
						),
						[
							myId, myPubData, myTitle, myAuthor, myArtData
						],
						function( transaction, results ){
							callback( results.insertId );
						}
					);
 
				}
			);
		};
		
		var saveSection = function( myId, myOrder, myName, cityFlag,mainSection, active,parentId  ){



  database.transaction(function(transaction){

	transaction.executeSql (

				(
	"INSERT INTO section (sectionId, displayOrder, sectionName, cityFlag,mainSection, active, parentId) VALUES (?,?,?,?,?,?,?)"
				 ),

				[
				 myId, myOrder, myName, cityFlag,mainSection, active, parentId
				 ],

				function(transaction, results){

				}

				);

	});




};
		
		var saveImages = function( artId, caption, image, callback ){
			database.transaction(
				function( transaction ){

					transaction.executeSql(
						(
							"INSERT INTO images (" +
								"articleId, caption, image" +
							" ) VALUES ( " +
								"?,?,? " +
							");"
						),
						[
							artId, caption, image
						],
						function( transaction, results ){
							callback( results.insertId );
						}
					);
 
				}
			);
		};
 

		var getArticle = function( callback ){
			database.transaction(
				function( transaction ){
					transaction.executeSql(
						(
							"SELECT " +
								"* " +
							"FROM " +
								"articles " +
							"ORDER BY " +
								"id DESC"
						),
						[],
						function( transaction, results ){
							callback( results );
						}
					);
				}
			);
		};
		
		
		var getSection = function( orderBy, callback ){
			
			database.transaction(
								 
				function( transaction ){
					transaction.executeSql(
						(
							"SELECT " +
								"* " +
							"FROM " +
								"section " +
							"ORDER BY " +
								" ? "
						),
						[ orderBy ],
						function( transaction, results ){
							callback( results );
						}
					);
				}
			);
		};
		

		var getParentSection = function( callback ){
    
    database.transaction(
			 
			 function(transaction){
				transaction.executeSql(
					(
	"SELECT a.sectionId as aid,a.sectionName as aname, a.parentId apid, a.cityFlag as city,a.active as active FROM section a where a.sectionName != 'Home'  and a.cityFlag = 0 or a.cityFlag = 2 order by a.displayOrder"
					 ),
					[
					 ],
					function(transaction, results){
						
						callback(results);
					}
				);	
			 }
			 
			 );
    
    
};
		
		var updateSection = function(active, sectionId, callback){
			database.transaction(
				function( transaction ){
					
					transaction.executeSql(
						(
							"UPDATE section " +
								"SET active=? where sectionId=?"
						),
						[ active, sectionId ],
						function(){
							//callback(sectionId);
							getSection("displayOrder", getSections);
						}
					);
 
				}
			);
		};
		
		var getImages = function( callback, artId ){
			
			database.transaction(
				function( transaction ){
					transaction.executeSql(
						(
							"SELECT " +
								"* " +
							"FROM " +
								"images " 
						),
						[],
						function( transaction, results ){
							//alert(results);
							callback( results, artId);
						}
					);
				}
			);
		};
		
		var getImagesbyID = function( artId, callback ){
			
			database.transaction(
				function( transaction ){
					transaction.executeSql(
						(
							"SELECT " +
								"* " +
							"FROM " +
								"images " +
							"where articleId=?" 
						),
						[ artId ],
						function( transaction, results ){
							//alert( results );
							callback( results );
						}
					);
				}
			);
		};

		var deleteArticle = function(artId, callback ){
			database.transaction(
				function( transaction ){

					transaction.executeSql(
						(
							"DELETE FROM " +
								"articles where articleId=?"
						),
						[ artId ],
						function(){
							callback(artId);
						}
					);
 
				}
			);
		};
		
		var deleteImages = function(artId, callback ){
			database.transaction(
				function( transaction ){

					transaction.executeSql(
						(
							"DELETE FROM " +
								"images where articleId=?"
						),
						[ artId ],
						function(){
							//callback(artId);
						}
					);
 
				}
			);
		};
 