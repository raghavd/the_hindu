
//////////////////// //////////////////////////////////////////
//////////////////// Font Resize script here //////////////////
//////////////////// //////////////////////////////////////////
 $(document).ready(function () {

    //min font size
     
      $("a.fontSizeMinus").css({ opacity: 0.5 });
	var min=14; 	

	//max font size
	var max=17;	
	
	//grab the default font size

	var reset = 14; 
	
	//font resize these elements
	var elm = $('#artData');  
	
	//set the default font size and remove px from the value
	var size = 14;
	
    
                   
	//Increase font size
	$('a.fontSizePlus').click(function() {
		//if the font size is lower or equal than the max value
		//alert("size = "+size);
		if (size<max) {
			 $("a.fontSizeMinus").css({ opacity: 1.0 });
			//increase the size
			size++;
			
            			//set the font size
			elm.css({'fontSize' : size});
                              
        if(size == max){
            $("a.fontSizePlus").css({ opacity: 0.5 });
            //return;
        }

			
		}
                              
//       else{
//            
//            $("a.fontSizePlus").css({ opacity: 0.5 });
//            
//        }
		
		resizeContentWindow();
		//cancel a click event
		return false;	
        
    });

    $('a.fontSizeMinus').click(function() {
 	//$("a.fontSizeMinus").css("-webkit-tap-highlight-color","transparent","outline","none");
//if the font size is greater or equal than min value
		if (size>min) {
			$("a.fontSizePlus").css({ opacity: 1.0 });
			//decrease the size
			size--;
			
			//set the font size
			elm.css({'fontSize' : size});
                               
                               
          if(size == min){
            
          $("a.fontSizeMinus").css({ opacity: 0.5 });
          //return;
          }
                               
		}
//        else{
//             $("a.fontSizeMinus").css({ opacity: 0.5 });
//            
//        }
		resizeContentWindow();
		//cancel a click event
		return false;	
        
    });
    
    //Reset the font size
    $('a.fontReset').click(function () {
        
        //set the default font size    
         elm.css({'fontSize' : reset});        
    });
        
});

//A string replace function
function str_replace(haystack, needle, replacement) {
	var temp = haystack.split(needle);
	return temp.join(replacement);
}
//////////////////// //////////////////////////////////////////
//////////////////// Font Resize script end ///////////////////
//////////////////// //////////////////////////////////////////